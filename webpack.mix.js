let mix = require('laravel-mix')
mix.js('src/js/app.js', 'web/js/')
   .sass('src/scss/app.scss', 'web/css/');